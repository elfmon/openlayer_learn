import 'ol/ol.css';
import {Map, View} from 'ol';
import OSM from 'ol/source/OSM';
import WMTSCapabilities from 'ol/format/WMTSCapabilities';
import WMSCapabilities from "ol/format/WMSCapabilities";
import WMTS, {optionsFromCapabilities as wmtsOptionsFromCapabilities} from "ol/source/WMTS";
import {Image as ImageLayer, Tile as TileLayer} from 'ol/layer';
import ImageWMS from 'ol/source/ImageWMS';
import {extend} from "ol/extent";
import {boundingExtent} from 'ol/extent';

const server = 'http://49.234.5.204:8080';
let map = null;
let wmsLayer = null;
let wmtsLayer = null;

//加载内置的openstreetmap
function openDefaultMap() {
    destoryMap();
    map = new Map({
        target: 'map',
        layers: [
            new TileLayer({
                source: new OSM()
            })
        ],
        view: new View({
            center: [0, 0],
            zoom: 0
        })
    });
}

function destoryMap() {
    if (map === null) return;

    let host = map.getTargetElement();
    if (host) {
        host.innerHTML = '';
    }
    map = null;
}

function getMapProject() {
    return map.getView().getProjection();
}

function loadWmtsCapabilities() {
    if (map !== null) {
        destoryMap();
    }

    //1、从远程地址 GetCapabilities
    fetch(server + '/service?REQUEST=GetCapabilities&SERVICE=WMTS').then(function (response) {
        return response.text();
    }).then(function (text) {
        //2、解析Capabilities
        var parser = new WMTSCapabilities();
        var result = parser.read(text);

        debugger;
        //罗列远程服务器的服务，是个集合的结构
        console.log(result.Contents.Layer);

        var options = wmtsOptionsFromCapabilities(result, {
            layer: result.Contents.Layer[1].Identifier,
            matrixSet: result.Contents.Layer[1].TileMatrixSetLink[0].TileMatrixSet
        });

        wmtsLayer = new TileLayer({
            opacity: 1,
            source: new WMTS(options)
        });

        map = new Map({
            target: 'map',
            layers: [
                wmtsLayer
            ],
            view: new View({
                center: [0, 0],
                zoom: 0
            })
        });
    });
}

function loadWmsCapabilities() {
    if (map === null) {
        openDefaultMap();
    }

    //1、从远程地址 GetCapabilities
    fetch(server + '/service?REQUEST=GetCapabilities').then(function (response) {
        return response.text();
    }).then(function (text) {
        //2、解析Capabilities
        var parser = new WMSCapabilities();
        var result = parser.read(text);

        debugger;
        //罗列远程服务器的服务，是个树形的结构
        console.log(result.Capability.Layer);

        //3、从解析的结果构建wms layer
        /*
        wmsLayer = new ImageLayer({
            extent: [-13884991, 2870341, -7455066, 6338219],
            source: new ImageWMS({
                url: 'https://ahocevar.com/geoserver/wms',
                params: {'LAYERS': 'topp:states'},
                ratio: 1,
                serverType: 'geoserver'
            })
        });
        */

        //请求图片的方法
        var getMapInfo = result.Capability.Request.GetMap;
        //取出资源去请求地址
        var resource = getMapInfo.DCPType[0].HTTP.Get.OnlineResource;
        //拼装请求的Layers，多个Layer之间通过 , 分割。
        var layers = result.Capability.Layer.Layer[0].Layer[1].Name;
        //获取当前地图的投影信息，叠加在同一份地图上面的图层，都要要求一致
        var project = getMapProject();
        //构建wmslayer的boundbox
        var extent = null;
        for (var i = 0; i < result.Capability.Layer.Layer[0].Layer[1].BoundingBox.length; i++) {
            var crs = result.Capability.Layer.Layer[0].Layer[1].BoundingBox[i].crs;
            if (crs === project.getCode()) {
                extent = result.Capability.Layer.Layer[0].Layer[1].BoundingBox[i].extent;
                break;
            }
        }

        wmsLayer = new ImageLayer({
            source: new ImageWMS({
                url: resource,
                params: {'LAYERS': layers}
            })
        });
        if (extent) {
            wmsLayer.setExtent(extent);
        }

        //4、将wms layer添加到地图中
        map.addLayer(wmsLayer);

        /*var options = wmsOptionsFromCapabilities(result, {
            layer: 'layer-7328',
            matrixSet: 'EPSG:3857'
        });

        map = new Map({
            layers: [
                new TileLayer({
                    source: new OSM(),
                    opacity: 0.7
                }),
                new TileLayer({
                    opacity: 1,
                    source: new WMTS(options)
                })
            ],
            target: 'map',
            view: new View({
                center: [19412406.33, -5050500.21],
                zoom: 5
            })
        });*/
    });
}

document.querySelector("#btnLoadDefaultMap").addEventListener('click', () => {
    openDefaultMap();
});
document.querySelector("#btnLoadWMTSMap").addEventListener('click', () => {
    loadWmtsCapabilities();
});
document.querySelector('#btnLoadWmsCapabilities').addEventListener('click', () => {
    loadWmsCapabilities();
});
